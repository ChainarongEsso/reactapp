import { productRef } from "../config/firebase";
import { FETCH_PRODUCTS } from "./type";

export const addProduct = newProduct => async dispatch => {
    productRef.push().set(newProduct);
};

export const deleteProduct = productId => async dispatch => {
    productRef.child(productId).remove();
};

export const updateProduct = (productId, newProduct) => async dispatch => {
    productRef.child(productId).update(newProduct);
}

export const fetchProducts = () => async dispatch => {
    productRef.on("value", snapshot => {
        dispatch({
            type: FETCH_PRODUCTS,
            payload: snapshot.val()
        });
    });
    
};