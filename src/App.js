import React, { Component } from 'react';
import ProductList from './component/productList';

class App extends Component {
  render() {
    return <ProductList />
  }
}

export default App;
