import * as firebase from "firebase";

firebase.initializeApp({
    apiKey: "AIzaSyDerqJ3RSF_OqfJUWKKm9ATHN2tRE8wkgA",
    authDomain: "react-app-47a36.firebaseapp.com",
    databaseURL: "https://react-app-47a36.firebaseio.com",
    projectId: "react-app-47a36",
    storageBucket: "react-app-47a36.appspot.com",
    messagingSenderId: "693285239279"
});

const databaseRef = firebase.database().ref();
export const productRef = databaseRef.child("products");