import React, { Component } from "react";
import { connect } from "react-redux";
import { deleteProduct } from "../actions";

class ProductListItem extends Component {
    onDeleteProduct = productId => {
    const { deleteProduct } = this.props;
    deleteProduct(productId);
  };

  render() {
    const { productId, product, editProduct } = this.props;
    return (
      <div key="productName" style={ { backgroundColor: "#d1e0e0", margin: 20, padding: 2 } }>
        <h4>
          {"ProductId "}{product.productId}
        </h4>
        <h4>
          {"ProductName "}{product.productName}{" ProductCategory "}{product.productCategory}
        </h4>
        <h4>
          {"ProductDateAdd "}{product.productDateAdd}{" ProductDateEdit "}{product.productDateEdit}
        </h4>
        <button onClick={() => editProduct(productId, product)} >
          <a>edit</a>
        </button>{" "}
        <button onClick={() => this.onDeleteProduct(productId)} >
          <a>delete</a>
        </button>
      </div>
    );
  }
}

export default connect(null, { deleteProduct })(ProductListItem);