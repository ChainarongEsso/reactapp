import React, { Component } from 'react';
import { connect } from 'react-redux';
import _ from "lodash";
import * as actions from '../actions';
import ProductListItem from './productListItem';

const initialState = {
    productKey: "",
    updateStatus: false,
    productId: "",
    productName: "",
    productCategory: "",
    productDateAdd: "",
    productDateEdit: "",
}

class ProductList extends Component {

    state = initialState;

    inputChange = status => event => {
        this.setState({ [status]: event.target.value });
    };

    formSubmit = event => {
        const {
            productId,
            productName,
            productCategory,
            productDateEdit,
        } = this.state;
        const { addProduct } = this.props;
        event.preventDefault();
        addProduct({ productId, productName, productCategory, productDateAdd: new Date().getTime(), productDateEdit });
        this.setState(initialState);
    }

    editSubmit = event => {
        const {
            productKey,
            productId,
            productName,
            productCategory,
            productDateAdd,
        } = this.state;
        const { updateProduct } = this.props;
        event.preventDefault();
        updateProduct(productKey, { productId, productName, productCategory, productDateAdd, productDateEdit: new Date().getTime() });
        this.setState(initialState);
    }

    editProduct = (key, product) => {
        this.setState({
            productKey: key,
            updateStatus: true,
            productId: product.productId,
            productName: product.productName,
            productCategory: product.productCategory,
            productDateAdd: product.productDateAdd,
            productDateEdit: new Date().getTime(),
        });
    }

    renderAddForm = () => {
        const {
            updateStatus,
            productId,
            productName,
            productCategory,
        } = this.state;
        return (
        <div>
            <form onSubmit={updateStatus ? this.editSubmit : this.formSubmit}>
                <div>
                    <div style={{marginBottom: 10}}>
                        <label style={{marginRight: 10}}>productId:</label>
                        <input
                        type="text"
                        value={productId}
                        onChange={this.inputChange('productId')} required/>
                    </div>
                    <div style={{marginBottom: 10}}>
                        <label style={{marginRight: 10}}>productName:</label>
                        <input
                        type="text"
                        value={productName}
                        onChange={this.inputChange('productName')} required/>
                    </div>
                    <div style={{marginBottom: 10}}>
                        <label  style={{marginRight: 10}}>productCategory:</label>
                        <input
                        type="text"
                        value={productCategory}
                        onChange={this.inputChange('productCategory')} required/>
                    </div>
                    <div style={{marginBottom: 10}}>
                        <button type="submit">{updateStatus ? ( <i>update</i> ) : ( <i>add</i> )}</button>
                    </div>
                </div>
            </form>
        </div>
        );
    };

    renderProducts() {
        const { products } = this.props;
        const productList = _.map(products, (value, key) => {
            return <ProductListItem key={key} productId={key} product={value} editProduct={this.editProduct} />;
        });
        if (!_.isEmpty(productList)) {
          return productList;
        }
        return (
          <div>
            <h4>You not have Product</h4>
            <p>Start by clicking add button in the bottom of the screen</p>
          </div>
        );
      }


    componentWillMount() {
        this.props.fetchProducts();
    }
    

    render() {
        return (
            <div style={ {textAlign: 'center'} }>
                {this.renderAddForm()}
                {this.renderProducts()}
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        products: state.product
    };
}

export default connect(mapStateToProps, actions)(ProductList);